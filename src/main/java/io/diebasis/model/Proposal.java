package io.diebasis.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Proposal
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-21T21:54:57.554Z[GMT]")


public class Proposal   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("pollId")
  private Long pollId = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("choices")
  @Valid
  private List<Choices> choices = new ArrayList<Choices>();

  public Proposal id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Proposal pollId(Long pollId) {
    this.pollId = pollId;
    return this;
  }

  /**
   * Get pollId
   * @return pollId
   **/
  @Schema(required = true, description = "")
      @NotNull

    public Long getPollId() {
    return pollId;
  }

  public void setPollId(Long pollId) {
    this.pollId = pollId;
  }

  public Proposal title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Proposal description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Proposal choices(List<Choices> choices) {
    this.choices = choices;
    return this;
  }

  public Proposal addChoicesItem(Choices choicesItem) {
    this.choices.add(choicesItem);
    return this;
  }

  /**
   * Get choices
   * @return choices
   **/
  @Schema(required = true, description = "")
      @NotNull
    @Valid
    public List<Choices> getChoices() {
    return choices;
  }

  public void setChoices(List<Choices> choices) {
    this.choices = choices;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Proposal proposal = (Proposal) o;
    return Objects.equals(this.id, proposal.id) &&
        Objects.equals(this.pollId, proposal.pollId) &&
        Objects.equals(this.title, proposal.title) &&
        Objects.equals(this.description, proposal.description) &&
        Objects.equals(this.choices, proposal.choices);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, pollId, title, description, choices);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Proposal {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    pollId: ").append(toIndentedString(pollId)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    choices: ").append(toIndentedString(choices)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

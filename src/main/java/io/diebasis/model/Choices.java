package io.diebasis.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;

/**
 * Choices
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-21T21:54:57.554Z[GMT]")


public class Choices   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("proposalId")
  private Long proposalId = null;

  /**
   * Gets or Sets choiceType
   */
  public enum ChoiceTypeEnum {
    SINGLE("single"),
    
    MULTI("multi"),
    
    NUMBER("number");

    private String value;

    ChoiceTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ChoiceTypeEnum fromValue(String text) {
      for (ChoiceTypeEnum b : ChoiceTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("choiceType")
  private ChoiceTypeEnum choiceType = null;

  public Choices id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Choices proposalId(Long proposalId) {
    this.proposalId = proposalId;
    return this;
  }

  /**
   * Get proposalId
   * @return proposalId
   **/
  @Schema(description = "")
  
    public Long getProposalId() {
    return proposalId;
  }

  public void setProposalId(Long proposalId) {
    this.proposalId = proposalId;
  }

  public Choices choiceType(ChoiceTypeEnum choiceType) {
    this.choiceType = choiceType;
    return this;
  }

  /**
   * Get choiceType
   * @return choiceType
   **/
  @Schema(description = "")
  
    public ChoiceTypeEnum getChoiceType() {
    return choiceType;
  }

  public void setChoiceType(ChoiceTypeEnum choiceType) {
    this.choiceType = choiceType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Choices choices = (Choices) o;
    return Objects.equals(this.id, choices.id) &&
        Objects.equals(this.proposalId, choices.proposalId) &&
        Objects.equals(this.choiceType, choices.choiceType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, proposalId, choiceType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Choices {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    proposalId: ").append(toIndentedString(proposalId)).append("\n");
    sb.append("    choiceType: ").append(toIndentedString(choiceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

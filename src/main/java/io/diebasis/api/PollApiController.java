package io.diebasis.api;

import io.diebasis.model.Poll;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-21T21:54:57.554Z[GMT]")
@RestController
public class PollApiController implements PollApi {

    private static final Logger log = LoggerFactory.getLogger(PollApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public PollApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> addPoll(@Parameter(in = ParameterIn.DEFAULT, description = "Poll object that needs to be added", required=true, schema=@Schema()) @Valid @RequestBody Poll body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> archivePoll(@Parameter(in = ParameterIn.PATH, description = "The poll id to archive", required=true, schema=@Schema()) @PathVariable("pollId") Long pollId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Poll>> findPollsByStatus(@NotNull @Parameter(in = ParameterIn.QUERY, description = "Status values that need to be considered for filter" ,required=true,schema=@Schema(allowableValues={ "draft", "active", "expired", "closed", "archive" }
)) @Valid @RequestParam(value = "status", required = true) String status) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Poll>>(objectMapper.readValue("[ {\n  \"proposal\" : [ {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  }, {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  } ],\n  \"owner\" : \"owner\",\n  \"expires\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"created\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"description\" : \"Please tell us your favourite flavour - is it vanilla or chocolate.\",\n  \"id\" : 0,\n  \"title\" : \"What is your favourite flavour?\",\n  \"status\" : \"draft\"\n}, {\n  \"proposal\" : [ {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  }, {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  } ],\n  \"owner\" : \"owner\",\n  \"expires\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"created\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"description\" : \"Please tell us your favourite flavour - is it vanilla or chocolate.\",\n  \"id\" : 0,\n  \"title\" : \"What is your favourite flavour?\",\n  \"status\" : \"draft\"\n} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Poll>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Poll>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Poll> getPollById(@Parameter(in = ParameterIn.PATH, description = "ID of poll to return", required=true, schema=@Schema()) @PathVariable("pollId") Long pollId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Poll>(objectMapper.readValue("{\n  \"proposal\" : [ {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  }, {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  } ],\n  \"owner\" : \"owner\",\n  \"expires\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"created\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"description\" : \"Please tell us your favourite flavour - is it vanilla or chocolate.\",\n  \"id\" : 0,\n  \"title\" : \"What is your favourite flavour?\",\n  \"status\" : \"draft\"\n}", Poll.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Poll>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Poll>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updatePoll(@Parameter(in = ParameterIn.DEFAULT, description = "Poll object with updated data for the poll", required=true, schema=@Schema()) @Valid @RequestBody Poll body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Poll> updatePollById(@Parameter(in = ParameterIn.PATH, description = "ID of poll that needs to be updated", required=true, schema=@Schema()) @PathVariable("pollId") Long pollId,@Parameter(in = ParameterIn.DEFAULT, description = "Poll object that needs to be added", required=true, schema=@Schema()) @Valid @RequestBody Poll body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Poll>(objectMapper.readValue("{\n  \"proposal\" : [ {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  }, {\n    \"pollId\" : 1,\n    \"description\" : \"description\",\n    \"id\" : 6,\n    \"title\" : \"title\",\n    \"choices\" : [ {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    }, {\n      \"choiceType\" : \"single\",\n      \"id\" : 5,\n      \"proposalId\" : 5\n    } ]\n  } ],\n  \"owner\" : \"owner\",\n  \"expires\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"created\" : \"2000-01-23T04:56:07.000+00:00\",\n  \"description\" : \"Please tell us your favourite flavour - is it vanilla or chocolate.\",\n  \"id\" : 0,\n  \"title\" : \"What is your favourite flavour?\",\n  \"status\" : \"draft\"\n}", Poll.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Poll>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Poll>(HttpStatus.NOT_IMPLEMENTED);
    }

}
